package ru.tsc.panteleev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Role;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}

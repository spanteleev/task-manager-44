package ru.tsc.panteleev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.model.IProjectRepository;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void set(@NotNull Collection<Project> projects) {
        clear();
        for (Project project : projects)
            add(project);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull String userId) {
        return entityManager.createQuery("FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort) {
        if (sort == null) return findAllByUserId(userId);
        @NotNull final String query =
                String.format("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.%s", getSortColumn(sort));
        return entityManager.createQuery(query, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull List<Project> findAll() {
        return entityManager.createQuery("FROM Project", Project.class).getResultList();
    }

    @Nullable
    @Override
    public Project findById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        remove(findById(userId, id));
    }

    @Override
    public void clearByUserId(@NotNull String userId) {
        entityManager.createQuery("DELETE FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Project", Project.class).executeUpdate();
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(p) FROM Project p", Long.class).getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return findById(userId, id) != null;
    }

}

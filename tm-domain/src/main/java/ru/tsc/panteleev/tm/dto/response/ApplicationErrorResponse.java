package ru.tsc.panteleev.tm.dto.response;

import org.jetbrains.annotations.NotNull;

public class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}

package ru.tsc.panteleev.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;

@NoArgsConstructor
public class TaskUnbindFromProjectResponse extends AbstractTaskResponse {

    public TaskUnbindFromProjectResponse(@Nullable TaskDTO task) {
        super(task);
    }

}

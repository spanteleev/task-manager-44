package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDTO;
import java.util.Collection;
import java.util.List;

public interface IServiceDTO<M extends AbstractModelDTO> {

    void set(@NotNull Collection<M> models);

    @Nullable
    List<M> findAll();

}

package ru.tsc.panteleev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.panteleev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.dto.model.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@NotNull final UserDTO user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

}
